import React from 'react';
import ReactDom from 'react-dom';

// Components 
import BadgeNew from './pages/badgenew';
import Badges from './pages/Badges';

// Styles
import 'bootstrap/dist/css/bootstrap.css';
import './global.css';

const container = document.getElementById('app');

ReactDom.render(
    <Badges  />, 
    container
);