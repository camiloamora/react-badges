import React from 'react';

// styles
import "./styles/BadgeNew.css";

import Navbar from '../components/Navbar';
import header from '../images/badge-header.svg';

import BadgeForm from '../components/BadgeForm';
import Badge from '../components/badge';

class BadgeNew extends React.Component {
    state = { form: {
        firstName: '',
        lastName: '',
        email: '',
        twitter: '',
        jobTitle: 'Designer'
    } }

    handleChange = e => {
        // Forma 1
        // const nextForm = this.state.form;
        // nextForm[e.target.name] = e.target.value;

        this.setState({
            // Forma 1
            // form: nextForm
            //Forma 2
            form: {
                ... this.state.form,
                [e.target.name]: e.target.value
            },
        })
    }

    render() {
       return (
           <div>
               <Navbar />
               <div className="BadgeNew__hero">
                    <img className="img-fluid" src={header} alt="Logo"/>
               </div>

               <div className="container">
                   <div className="row">
                       <div className="col-6">
                           <Badge 
                            firstName={this.state.form.firstName}
                            lastName={this.state.form.lastName}
                            twitter={this.state.form.twitter}
                            jobTitle={this.state.form.jobTitle}
                            email={this.state.form.email}
                            avatarUrl="https://secure.gravatar.com/avatar/1c639f85a92193b84019cb2ee85fb486"
                           />
                       </div>

                       <div className="col-6">
                           <BadgeForm onChange={this.handleChange} formValues={this.state.form}/>
                       </div>
                   </div>
               </div>
           </div>
       );
    }
}

export default BadgeNew;