import React from 'react';

class BadgeForm extends React.Component {
    // state = {
    //     firstName: '',
    //     lastName: '',
    //     email: '',
    //     twitter: '',
    //     jobTitle: 'Designer'
    // };
    handleChange = e=> {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleClick = e => {
        e.preventDefault();
        console.log('button log click');
        console.log(this.state);
        
    }

    handleSubmite = e => {
        e.preventDefault();
    }

    render() {
        return (
            <div>
                <h1>
                    New Attendant
                </h1>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>First Name</label>
                        <input onChange={this.props.handleChange} className="form-control" type="text" 
                        name="firstName"
                        value={this.props.formValues.firstName}
                        />
                    </div>
                    <div className="form-group">
                        <label>Last Name</label>
                        <input onChange={this.props.handleChange} className="form-control" type="text" 
                        name="lastName"
                        value={this.props.formValues.lastName}
                        />
                    </div>
                    <div className="form-group">
                        <label>Email</label>
                        <input onChange={this.props.handleChange} className="form-control" type="email" 
                        name="email"
                        value={this.props.formValues.email}
                        />
                    </div>
                    <div className="form-group">
                        <label>job title</label>
                        <input onChange={this.props.handleChange} className="form-control" type="text" 
                        name="jobTitle"
                        value={this.props.formValues.jobTitle}
                        />
                    </div>
                    <div className="form-group">
                        <label>Twitter</label>
                        <input onChange={this.props.handleChange} className="form-control" type="text" 
                        name="twitter"
                        value={this.props.formValues.twitter}
                        />
                    </div>
                    <button className="btn btn-primary" onClick={this.handleClick}>
                        Save
                    </button>
                </form>
            </div>
        )
    }
}

export default BadgeForm;