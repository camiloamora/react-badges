import React from 'react';
import confLogo from '../images/badge-header.svg';
import "./styles/badge.css";

class Badge extends React.Component {
    // Metodo obligatorio
    render() {
        
        return (
            <div className="Badge">
                <div className="Badge__header">
                    <img src={confLogo} alt="Logo de la conferencia"></img>
                </div>

                <div className="Badge__section-name">
                    <img src={ this.props.avatarUrl } 
                    alt="Avatar" className="Badge__avatar"/>
                    <h3>
                        { this.props.firstName } <br/> { this.props.lastName }
                    </h3>
                </div>

                <div className="Badge__section-info">
                    <p>{ this.props.jobTitle }</p>
                    <div>@{ this.props.twitter }</div>
                </div>

                <div className="Badge__footer">
                    #platziconf
                </div>
            </div>
        )
    }
}

export default Badge;